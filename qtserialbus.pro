########################################
########################################
#   Qt 自定义Modbus模块模块，添加Ascii的支持
########################################
########################################

requires(qtHaveModule(serialport))

lessThan(QT_MAJOR_VERSION, 5) {
    message("Cannot build current QtSerialBus sources with Qt version $${QT_VERSION}.")
}

load(qt_parts)
