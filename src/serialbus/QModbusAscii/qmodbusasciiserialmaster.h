#ifndef QMODBUSASCIISERIALMASTER_H
#define QMODBUSASCIISERIALMASTER_H

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QtSerialBus/qmodbusclient.h>

class QModbusAsciiSerialMasterPrivate;

/**
 * @brief The QModbusAsciiSerialMaster class
 *
 * 添加的对modebus ascii 的支持类，实现与QModbusRtuSerialMaster一致
 *
 * 关于modebus ascii的说明
 * MODBUS协议详解
 *  http://www.360doc.com/content/16/1110/00/1355383_605294657.shtml
 *
 *  <----------------------------ADU----------------------------------->
 *                  <------------PDU------------->
 *  |  :  |  addr   |  fcode  |        data      |   LRC  |   \r\n  |
 *   起始符   地址       功能            数据           校验     结束符
 *   1字符    2字符      2字符        0到2*252字符      2字符    2字符
 *
 *   3A | 30 31 | 30 33 | 31 34 30 30 30 30 30 30 30 31 30 30 30 32 30 30 30 33 30 30 30 34 30 30 30 35 30 30 30 36 30 30 30 37 30 30 30 38 30 30 30 39 42 42 0D 0A
 *    : |  0  1 |  0  3 |  1  4  0  0  0  0  0  0  0  1  0  0  0  2  0  0  0  3  0  0  0  4  0  0  0  5  0  0  0  6  0  0  0  7  0  0  0  8  0  0  0  9  B  B \r \n
 */
class Q_SERIALBUS_EXPORT QModbusAsciiSerialMaster : public QModbusClient
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QModbusAsciiSerialMaster)

public:
    explicit QModbusAsciiSerialMaster(QObject *parent = nullptr);
    ~QModbusAsciiSerialMaster();

    int interFrameDelay() const;
    void setInterFrameDelay(int microseconds);

protected:
    QModbusAsciiSerialMaster(QModbusAsciiSerialMasterPrivate &dd, QObject *parent = nullptr);

    void close() override;
    bool open() override;
};

#endif // QMODBUSASCIISERIALMASTER_H
