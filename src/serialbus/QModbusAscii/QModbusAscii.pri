
INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/qmodbusasciiserialmaster.h \
    $$PWD/qmodbusasciiserialslave.h \
    $$PWD/qmodbusasciiserialmaster_p.h

SOURCES += \
    $$PWD/qmodbusasciiserialmaster.cpp \
    $$PWD/qmodbusasciiserialslave.cpp
