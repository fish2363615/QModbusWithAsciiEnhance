#ifndef QMODBUSASCIISERIALSLAVE_H
#define QMODBUSASCIISERIALSLAVE_H

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QtSerialBus/qmodbusserver.h>

class QModbusAsciiSerialSlave : public QModbusServer
{
public:
    QModbusAsciiSerialSlave();
};

#endif // QMODBUSASCIISERIALSLAVE_H
