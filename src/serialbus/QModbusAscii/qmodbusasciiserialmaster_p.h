﻿//#if _MSC_VER >= 1600
//#pragma execution_character_set("utf-8")
//#endif

#ifndef QMODBUSASCIISERIALMASTERPRIVATE_H
#define QMODBUSASCIISERIALMASTERPRIVATE_H

#include <QtCore/qloggingcategory.h>
#include <QtCore/qmath.h>
#include <QtCore/qpointer.h>
#include <QtCore/qqueue.h>
#include <QtCore/qtimer.h>
#include "QModbusAsciiSerialMaster.h"
#include <QtSerialPort/qserialport.h>

#include <private/qmodbusadu_p.h>
#include <private/qmodbusclient_p.h>
#include <private/qmodbus_symbols_p.h>

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API. It exists purely as an
// implementation detail. This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

QT_BEGIN_NAMESPACE

Q_DECLARE_LOGGING_CATEGORY(QT_MODBUS)
Q_DECLARE_LOGGING_CATEGORY(QT_MODBUS_LOW)


/**
 * @brief The QModbusAsciiSerialMasterPrivate class
 */

class QModbusAsciiSerialMasterPrivate : public QModbusClientPrivate
{
    Q_DECLARE_PUBLIC(QModbusAsciiSerialMaster)
    enum State {
        Idle,     //空闲
        Schedule, //分配
        Send,     //发送
        Receive,  //接收
    } m_state = Idle;

public:

    /**
     * @brief setupSerialPort  配置好串口参数
     */
    void setupSerialPort()
    {
        //绑定一些串口操作

        Q_Q(QModbusAsciiSerialMaster);

        m_sendTimer.setSingleShot(true);
        QObject::connect(&m_sendTimer, &QTimer::timeout, q, [this]() { processQueue(); });

        m_responseTimer.setSingleShot(true);
        QObject::connect(&m_responseTimer, &QTimer::timeout, q, [this]() { processQueue(); });

        m_serialPort = new QSerialPort(q);
        //读取串口来的数据
        QObject::connect(m_serialPort, &QSerialPort::readyRead, q, [this]()
        {
            //读取返回数据到回复缓存
            responseBuffer += m_serialPort->read(m_serialPort->bytesAvailable());
            qCDebug(QT_MODBUS_LOW) << "(ASCII client) Response buffer:" << responseBuffer;//.toHex();

            //数据没有接收完全，接着接收数据 以':'开头 以"\r\n" 结尾 且PDU部分不可能出现该结束符
            int startIdx = responseBuffer.indexOf(':');
            int endIdx = responseBuffer.indexOf("\r\n");
            if ( startIdx < 0 || endIdx < 0 )
            {
                qCDebug(QT_MODBUS) << "(ASCII client) Modbus ADU not complete";
                return;
            }

            //NOTE:切换数据到ascii模式,构建应用数据单元
            const QModbusSerialAdu tmpAdu(QModbusSerialAdu::Ascii, responseBuffer.mid(startIdx,endIdx-startIdx+2));
            int pduSizeWithoutFcode = QModbusResponse::calculateDataSize(tmpAdu.pdu());

            //检查协议数据单元大小
            if (pduSizeWithoutFcode < 0)
            {
                // wait for more data
                qCDebug(QT_MODBUS) << "(ASCII client) Cannot calculate PDU size for function code:"
                                   << tmpAdu.pdu().functionCode() << ", delaying pending frame";
                return;
            }

            //计算数据大小 非必要
            // start byte + server address byte + function code byte + PDU size + 2 bytes LRC
//            int aduSize = (1 + 2 + pduSizeWithoutFcode + 2 + 2)*2;
//            if (tmpAdu.rawSize() < aduSize)
//            {
//                qCDebug(QT_MODBUS) << "(ASCII client) Incomplete ADU received, ignoring";
//                return;
//            }

            //未实现诊断功能
            // Special case for Diagnostics:ReturnQueryData. The response has no
            // length indicator and is just a simple echo of what we have send.
            if (tmpAdu.pdu().functionCode() == QModbusPdu::Diagnostics)
            {
                const QModbusResponse response = tmpAdu.pdu();
                if (canMatchRequestAndResponse(response, tmpAdu.serverAddress()))
                {
                    quint16 subCode = 0xffff;
                    response.decodeData(&subCode);
                    if (subCode == Diagnostics::ReturnQueryData)
                    {
                        if (response.data() != m_current.requestPdu.data())
                            return; // echo does not match request yet
//                        aduSize = 2 + response.dataSize() + 2;
//                        if (tmpAdu.rawSize() < aduSize)
//                            return; // echo matches, probably checksum missing
                    }
                }
            }

            const QModbusSerialAdu adu = tmpAdu;
            //拿取必要的数据
            responseBuffer.remove(0, endIdx+2+1);

            //检查是否有数据剩余
            qCDebug(QT_MODBUS)<< "(ASCII client) Received ADU:" << adu.rawData();//.toHex();
            if (QT_MODBUS().isDebugEnabled() && !responseBuffer.isEmpty())
                qCDebug(QT_MODBUS_LOW) << "(ASCII client) Pending buffer:" << responseBuffer;//.toHex();

            // check LRC
            if (!adu.matchingChecksum())
            {
                qCWarning(QT_MODBUS) << "(ASCII client) Discarding response with wrong LRC, received:"
                                     << adu.checksum<quint8>() << ", calculated LRC:"
                                     << QModbusSerialAdu::calculateLRC(adu.data(), adu.size());
                return;
            }

            const QModbusResponse response = adu.pdu();
            if (!canMatchRequestAndResponse(response, adu.serverAddress()))
            {
                qCWarning(QT_MODBUS) << "(ASCII client) Cannot match response with open request, "
                    "ignoring";
                return;
            }

            if (m_state != State::Receive)
                return;

            m_sendTimer.stop();
            m_responseTimer.stop();
            processQueueElement(response, m_current);

            m_state = Schedule; // reschedule, even if empty
            m_serialPort->clear(QSerialPort::AllDirections);
            QTimer::singleShot(m_interFrameDelayMilliseconds, [this]() { processQueue(); });
        });

        using TypeId = void (QSerialPort::*)(QSerialPort::SerialPortError);
        QObject::connect(m_serialPort, static_cast<TypeId>(&QSerialPort::error),
                         [this](QSerialPort::SerialPortError error) {
            if (error == QSerialPort::NoError)
                return;

            qCDebug(QT_MODBUS) << "(ASCII server) QSerialPort error:" << error
                               << (m_serialPort ? m_serialPort->errorString() : QString());

            Q_Q(QModbusAsciiSerialMaster);

            switch (error) {
            case QSerialPort::DeviceNotFoundError:
                q->setError(QModbusDevice::tr("Referenced serial device does not exist."),
                            QModbusDevice::ConnectionError);
                break;
            case QSerialPort::PermissionError:
                q->setError(QModbusDevice::tr("Cannot open serial device due to permissions."),
                            QModbusDevice::ConnectionError);
                break;
            case QSerialPort::OpenError:
            case QSerialPort::NotOpenError:
                q->setError(QModbusDevice::tr("Cannot open serial device."),
                            QModbusDevice::ConnectionError);
                break;
            case QSerialPort::WriteError:
                q->setError(QModbusDevice::tr("Write error."), QModbusDevice::WriteError);
                break;
            case QSerialPort::ReadError:
                q->setError(QModbusDevice::tr("Read error."), QModbusDevice::ReadError);
                break;
            case QSerialPort::ResourceError:
                q->setError(QModbusDevice::tr("Resource error."), QModbusDevice::ConnectionError);
                break;
            case QSerialPort::UnsupportedOperationError:
                q->setError(QModbusDevice::tr("Device operation is not supported error."),
                            QModbusDevice::ConfigurationError);
                break;
            case QSerialPort::TimeoutError:
                q->setError(QModbusDevice::tr("Timeout error."), QModbusDevice::TimeoutError);
                break;
            case QSerialPort::UnknownError:
                q->setError(QModbusDevice::tr("Unknown error."), QModbusDevice::UnknownError);
                break;
            default:
                qCDebug(QT_MODBUS) << "(ASCII server) Unhandled QSerialPort error" << error;
                break;
            }
        });

        //统计发送的字节数
        QObject::connect(m_serialPort, &QSerialPort::bytesWritten, q, [this](qint64 bytes) {
            m_current.bytesWritten += bytes;
        });

        //串口关闭时清理数据
        QObject::connect(m_serialPort, &QSerialPort::aboutToClose, q, [this]() {
            Q_Q(QModbusAsciiSerialMaster);
            Q_UNUSED(q); // To avoid unused variable warning in release mode
            Q_ASSERT(q->state() == QModbusDevice::ClosingState);

            m_sendTimer.stop();
            m_responseTimer.stop();
        });
    }

    /*!
        According to the Modbus specification, in ASCII mode message frames
        are separated by a silent interval of at least 3.5 character times.
        Calculate the timeout if we are less than 19200 baud, use a fixed
        timeout for everything equal or greater than 19200 baud.
        If the user set the timeout to be longer than the calculated one,
        we'll keep the user defined.
    */
    void calculateInterFrameDelay() {
        // The spec recommends a timeout value of 1.750 msec. Without such
        // precise single-shot timers use a approximated value of 1.750 msec.
        int delayMilliSeconds = 2;
        if (m_baudRate < 19200) {
            // Example: 9600 baud, 11 bit per packet -> 872 char/sec
            // so: 1000 ms / 872 char = 1.147 ms/char * 3.5 character
            // Always round up because the spec requests at least 3.5 char.
            delayMilliSeconds = qCeil(3500. / (qreal(m_baudRate) / 11.));
        }
        if (m_interFrameDelayMilliseconds < delayMilliSeconds)
            m_interFrameDelayMilliseconds = delayMilliSeconds;
    }

    void setupEnvironment() {
        if (m_serialPort) {
            m_serialPort->setPortName(m_comPort);
            m_serialPort->setParity(m_parity);
            m_serialPort->setBaudRate(m_baudRate);
            m_serialPort->setDataBits(m_dataBits);
            m_serialPort->setStopBits(m_stopBits);
        }

        calculateInterFrameDelay();

        responseBuffer.clear();
        m_state = QModbusAsciiSerialMasterPrivate::Idle;
    }

    /**
     * @brief scheduleNextRequest 切换到下一个任务
     */
    void scheduleNextRequest()
    {
        m_state = Schedule;
        m_serialPort->clear(QSerialPort::AllDirections);
        QTimer::singleShot(m_interFrameDelayMilliseconds, [this]() { processQueue(); });
    }

    QModbusReply *enqueueRequest(const QModbusRequest &request, int serverAddress,
        const QModbusDataUnit &unit, QModbusReply::ReplyType type) override
    {
        Q_Q(QModbusAsciiSerialMaster);

        auto reply = new QModbusReply(type, serverAddress, q);
        QueueElement element(reply, request, unit, m_numberOfRetries + 1);
        //NOTE: Modbus ascii 只支持 0~9 A~F
        element.adu = QModbusSerialAdu::create(QModbusSerialAdu::Ascii, serverAddress, request).toUpper();
        m_queue.enqueue(element);

        if (m_state == Idle)
            scheduleNextRequest();
        return reply;
    }

    /**
     * @brief processQueue  处理队列，没懂啥意思
     */
    void processQueue()
    {

        //保证定时器没有活动
        Q_ASSERT_X(!m_sendTimer.isActive(), "processQueue", "send timer active");
        Q_ASSERT_X(!m_responseTimer.isActive(), "processQueue", "response timer active");

        auto writeAdu = [this]() {
            responseBuffer.clear();
            m_current.bytesWritten = 0;
            m_current.numberOfRetries--;
            m_serialPort->write(m_current.adu);
            m_sendTimer.start(m_interFrameDelayMilliseconds);

            qCDebug(QT_MODBUS) << "(ASCII client) Sent Serial PDU:" << m_current.requestPdu;
            qCDebug(QT_MODBUS_LOW).noquote() << "(ASCII client) Sent Serial ADU: 0x" + m_current.adu
                .toHex();
        };

        //判断状态
        switch (m_state)
        {
            case Schedule:  //准备好下一个任务
                m_current = QueueElement();
                if (!m_queue.isEmpty())
                {
                    m_current = m_queue.dequeue();
                    if (m_current.reply)
                    {
                        m_state = Send;
                        QTimer::singleShot(0, [writeAdu]() { writeAdu(); });
                    }
                    else
                    {
                        QTimer::singleShot(0, [this]() { processQueue(); });
                    }
                }
                else
                {
                    m_state = Idle;
                }
                break;

            case Send:
                // send timeout will always happen
                if (m_current.reply.isNull())
                {
                    scheduleNextRequest();
                }
                else if (m_current.bytesWritten < m_current.adu.size()) //判断发送的数据量是否等于实际的数据量
                {
                    qCDebug(QT_MODBUS) << "(ASCII client) Send failed:" << m_current.requestPdu;

                    if (m_current.numberOfRetries <= 0)
                    {
                        if (m_current.reply)
                        {
                            m_current.reply->setError(QModbusDevice::TimeoutError,
                                QModbusClient::tr("Request timeout."));
                        }
                        m_current = QueueElement();
                        scheduleNextRequest();
                    }
                    else
                    {
                        m_serialPort->clear(QSerialPort::AllDirections);
                        QTimer::singleShot(m_interFrameDelayMilliseconds, [writeAdu]() { writeAdu(); });
                    }
                }
                else
                {
                    qCDebug(QT_MODBUS) << "(ASCII client) Send successful:" << m_current.requestPdu;
                    m_state = Receive;
                    m_responseTimer.start(m_responseTimeoutDuration);
                }
                break;

            case Receive:
                // receive timeout will only happen after successful send
                qCDebug(QT_MODBUS) << "(ASCII client) Receive timeout:" << m_current.requestPdu;
                if (m_current.reply.isNull())
                {
                    scheduleNextRequest();
                }
                else if (m_current.numberOfRetries <= 0)
                {
                    if (m_current.reply)
                    {
                        m_current.reply->setError(QModbusDevice::TimeoutError,
                            QModbusClient::tr("Response timeout."));
                    }
                    scheduleNextRequest();
                }
                else
                {
                    m_state = Send;
                    m_serialPort->clear(QSerialPort::AllDirections);
                    QTimer::singleShot(m_interFrameDelayMilliseconds, [writeAdu]() { writeAdu(); });
                }
                break;

            case Idle:
            default:
                Q_ASSERT_X(false, "processQueue", QByteArray("unexpected state: ").append(m_state));
                break;
        }
    }

    bool canMatchRequestAndResponse(const QModbusResponse &response, int sendingServer) const
    {
        if (m_current.reply.isNull())
            return false;   // reply deleted
        if (m_current.reply->serverAddress() != sendingServer)
            return false;   // server mismatch
        if (m_current.requestPdu.functionCode() != response.functionCode())
            return false;   // request for different function code
        return true;
    }

    bool isOpen() const override
    {
        if (m_serialPort)
            return m_serialPort->isOpen();
        return false;
    }

    QTimer m_sendTimer;  ///< 数据发送定时器
    QTimer m_responseTimer; ///<回复定时器

    QueueElement m_current;
    QByteArray responseBuffer;

    QQueue<QueueElement> m_queue;  //需要发送的数据队列
    QSerialPort *m_serialPort = nullptr;

    int m_interFrameDelayMilliseconds = 2; // A approximated value of 1.750 msec.
};

QT_END_NAMESPACE


#endif // QMODBUSASCIISERIALMASTERPRIVATE_H
